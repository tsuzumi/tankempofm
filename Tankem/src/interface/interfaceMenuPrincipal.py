## -*- coding: utf-8 -*-
from direct.showbase.ShowBase import ShowBase
from direct.gui.OnscreenText import OnscreenText 
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.interval.LerpInterval import *
from direct.interval.IntervalGlobal import *
from direct.showbase.Transitions import Transitions
import sys
import dependance
from DAOMaps import *
from DTOMaps import *
class InterfaceMenuPrincipal(ShowBase):
    def __init__(self):

        #Image d'arrière plan
        self.background=OnscreenImage(parent=render2d, image="../asset/Menu/menuPrincipal.jpg")

        #On dit à la caméra que le dernier modèle doit s'afficher toujours en arrière
        self.baseSort = base.cam.node().getDisplayRegion(0).getSort()
        base.cam.node().getDisplayRegion(0).setSort(20)

        #Titre du jeu
        self.textTitre = OnscreenText(text = "Tankem!",
                                      pos = (0,0.75), 
                                      scale = 0.32,
                                      fg=(0.8,0.9,0.7,1),
                                      align=TextNode.ACenter)
        
        dao = DAOMaps()
        maps=dao.readDb()
        
        '''
        maps = []
        data = []
        data.append([0,0,0,0,0,0])
        data.append([0,0,0,0,0,0])
        data.append([0,0,2,0,0,0])
        data.append([0,0,0,3,0,0])
        data.append([0,0,0,0,0,0])
        data.append([0,0,0,0,0,0])
        
        for i in range(4):
            map = DTOMaps()
            map.setMapId(i)
            map.setMapNom("map_default_" +str(i))
            map.setMapDateCreation("2016-03-16")
            map.setStatus("actif")
            map.setDelaisItem(10,20)
            map.setMapData(data)
            map.setMapSizeX(6)
            map.setMapSizeY(6)
            map.setSpawn1(1,1)
            map.setSpawn2(3,3)
            maps.append(map)
        '''

        mapCount = len(maps) 

        self.selectedMap = None
            
        #Boutons
        btnScale = (0.18,0.18)
        text_scale = 0.12
        borderW = (0.04, 0.04)
        couleurBack = (0.243,0.325,0.121,1)
        separation = 0.5
        hauteur = -0.6
        
        buttonText = "randomMap"
        self.b1 = DirectButton(text = buttonText,
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          extraArgs = [buttonText,maps], ###### EXTRA ARGS permet d'envoyer un objet/variable au messenger
                          ################################ pour le recevoir dans le game logic (voir charger jeu)
                          command=self.chargeJeu,
                          pos = (-separation,0,hauteur))


        self.b2 = DirectButton(text = "Quitter",
                          text_scale=btnScale,
                          borderWidth = borderW,
                          text_bg=couleurBack,
                          frameColor=couleurBack,
                          relief=2,
                          command = lambda : sys.exit(),
                          pos = (separation,0,hauteur))
        
        self.mapListButtons = []

        for i in range(mapCount):
            nom = maps[i].getMapNom()
            item = DirectButton(text = nom,
                    text_scale=0.1,
                    borderWidth = (0.01, 0.01),
                    frameColor=couleurBack,
                    text_align = TextNode.ALeft,
                    relief=2,
                    extraArgs = [i,maps],###### EXTRA ARGS permet d'envoyer un objet/variable au messenger
                    ######################## pour le recevoir dans le game logic (voir charger jeu)
                    command = self.chargeJeu)
            
            self.mapListButtons.append(item)
        
        numItemsVisible = 4
        itemHeight = 0.15
         
        self.myScrolledList = DirectScrolledList(
            decButton_pos= (0.35, 0, 0.6),
            decButton_text = "up",
            decButton_text_scale = 0.05,
            decButton_borderWidth = (0.005, 0.005),
         
            incButton_pos= (0.35, 0, -0.2),
            incButton_text = "down",
            incButton_text_scale = 0.05,
            incButton_borderWidth = (0.005, 0.005),
         
            pos = (-1, 0, 0),
            items = self.mapListButtons,
            numItemsVisible = numItemsVisible,
            forceHeight = itemHeight,
            itemFrame_pos = (0.1, 0, 0.4),
            )
        
        self.myScrolledList.setPos(-0.35,0,0)
        
        
        #Initialisation de l'effet de transition
        curtain = loader.loadTexture("../asset/Menu/loading.jpg")

        self.transition = Transitions(loader)
        self.transition.setFadeColor(0, 0, 0)
        self.transition.setFadeModel(curtain)

        self.sound = loader.loadSfx("../asset/Menu/demarrage.mp3")
            
    def cacher(self):
            #Est esssentiellement un code de "loading"

            #On remet la caméra comme avant
            base.cam.node().getDisplayRegion(0).setSort(self.baseSort)
            #On cache les menus
            self.background.hide()
            self.b1.hide()
            self.b2.hide()
            self.textTitre.hide()
            self.myScrolledList.hide()
        
    
    def chargeJeu(self,args,maps): 
            Sequence(Func(lambda : self.transition.irisOut(0.2)),
                     SoundInterval(self.sound),
                     Func(self.cacher),
                     Func(lambda : messenger.send("DemarrerPartie",[args,maps])), #### contien le nom du bouton appuye
                     ############################################################ permet de savoir dans le game logic si une map aleatoire ou une map de la liste a ete choisi
                     Wait(0.2), #Bug étrange quand on met pas ça. L'effet de transition doit lagger
                     Func(lambda : self.transition.irisIn(0.2))
            ).start()
