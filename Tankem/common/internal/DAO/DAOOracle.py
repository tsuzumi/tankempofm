#This script attempts to  look into a oracle database and then store the content in a csv location picked by the user
#This script assumes the following database model

#A table  containing the following entry and one number (the current value) associated to it note:spaces are irrelevant, use them or don't. CANNOT BE NULL
####################################################################################################################################################################
### vitesseChar,vitesseRotationChar,pointsVieChar,tempsMouvementBloc,canonitesseBalle,canonTempsRecharge,mitrailleteVitesseBalle,mitrailletteTempsRecharge,     ###
### grenadeVitesseInitialeBalle,grenadetempsRecharge,shotgunVitesseBalle,shotgunOuvertureFusil,shotgunTempsRecharge,piegeVitesseBalle,                           ###
### piegeTempsRecharge,missileGuideVitesseBalle,missileGuideTempsRecharge,springVitesseInitialeSaut,springTempsRecharge,rayonExplosionBalles,messageAccueilDuree,###
### messageCompteRebour                                                                                                                                          ###
####################################################################################################################################################################


#A table containing the following entry and one string(the current value) associated to it note:spaces are irrelevant, use them or don't. CANNOT BE NULL
####################################################################################################################################################################
### messageSignalDebutContenu,messageFinPartieContenu,messageAccueilContenu                                                                                      ###
####################################################################################################################################################################


import cx_Oracle
import csv
import ConnectInfo
from DTOBalance import *
ip = 'projetoracle.c2cewry96sln.us-west-2.rds.amazonaws.com'
port = 1521
SID = 'ORCL'
dsn_tns = cx_Oracle.makedsn(ip, port, SID)

class DAOOracle:
	def __init__(self):
		self.con=None
		
	def read(self):
		self.con  = cx_Oracle.connect('Main', 'Dietwater', dsn_tns)
		
		if(self.con ==None):
			print("Error, database cannot be reached, Using Default Values")
			dbInfo=DTOBalance()
			return dbInfo
		dbInfo=DTOBalance() #the object we will use to store db info at runtime
		cur=self.con.cursor()
		#Number balance reading
		cur.execute("SELECT donne,valeur FROM balance")

		stringNames=[]
		numberNames=[]
		
		for results in cur:
			print(results)
			varName=results[0]
			varName=varName.strip()
			varValue=results[1]
			if(dbInfo.getNum(varName)==None):
				print("Naming convention explained in script header is not respected in the database : variable "+varName + " will not be read")
			else:
				numberNames.append(varName)
				dbInfo.setNum(varName,varValue)

		#String balance reading
		cur.execute("SELECT donne,valeur FROM messages")
		for results in cur:
			print(results)
			varName=results[0]
			varName=varName.strip()
			varValue=results[1]
			if(dbInfo.getText(varName)==None):
				print("Naming convention explained in script header is not respected in the database : variable "+varName + " will not be read")
			else:
				stringNames.append(varName)
				dbInfo.setText(varName,varValue)

		cur.close()
		self.con.close()
		return dbInfo
	def write(self,dbInfo):	
		if(dbInfo==None):
			return
		self.con  = cx_Oracle.connect('Main', 'Dietwater', dsn_tns)
		if(self.con ==None):			
			print("Error, database cannot be reached")
			return
		cur=self.con.cursor()
		print(dbInfo.getNum("piegeVitesseBalle"))
		queryNumberTemplate='UPDATE '+ ConnectInfo.TABLE_NUMBER_NAME + " SET "+ ConnectInfo.TABLE_VALUE + " = "
		queryStringTemplate='UPDATE '+ ConnectInfo.TABLE_STRING_NAME + " SET "+ ConnectInfo.TABLE_VALUE + " = "
		whereTemplate=" WHERE "+ ConnectInfo.TABLE_VALUE_NAME + " = "
		for name in dbInfo.getNamesNum():
			query=queryNumberTemplate+ str(dbInfo.getNum(name))+whereTemplate + "'"+name+"'"
			cur.execute(query)
		for name in dbInfo.getNamesText():
			query=queryStringTemplate+"'"+str(dbInfo.getText(name))+"'"+whereTemplate + "'"+name+"'"
			
			cur.execute(query)
		
		cur.execute("commit")		
		cur.execute("SELECT * FROM messages")

		self.con.close()
