CREATE TABLE maps 
(
	ID int NOT NULL,
	nom VARCHAR2 (20),
	dateCreation VARCHAR2(30),
	status VARCHAR2(30),
	delaisItem NUMBER,
	spawn1_x NUMBER,
	spawn1_y NUMBER,
	spawn2_x NUMBER,
	spawn2_y NUMBER,
	sizeX NUMBER,
	sizeY NUMBER,
	PRIMARY KEY (id)
);
CREATE TABLE mapsData(
	id_map NUMBER,
	x NUMBER,
	y NUMBER,
	tile NUMBER,
	FOREIGN KEY (id_map) REFERENCES maps(id)
);
CREATE SEQUENCE seq_map_id
  MINVALUE 1
  START WITH 1
  INCREMENT BY 1
  CACHE 20;

commit;