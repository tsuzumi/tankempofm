import dependance
import DTOMaps
import ConnectInfo
import cx_Oracle

TEXTFILE_PATH = "mapSave.txt"
ip = 'projetoracle.c2cewry96sln.us-west-2.rds.amazonaws.com'
port = 1521
SID = 'ORCL'
dsn_tns = cx_Oracle.makedsn(ip, port, SID)

class DAOMaps:
    def __init__(self):
        pass

    def readText(self):
        print("test")
        with open(TEXTFILE_PATH, 'r') as f:
            data = f.read()
            dto = DTOMaps.DTOMaps()
            # remove empty column
            data = data[:-2]
            # split the columns
            data = data.split('|')
            data[0]=data[0].split(";")
            dto.setMapNom(data[0][0])
            dto.setMapDateCreation(data[0][1])
            dto.setDelaisItem(data[0][3],data[0][4])
            dto.setSpawn1(data[0][5],data[0][6])
            dto.setSpawn2(data[0][7],data[0][8])
            if (data[0][2] == "0"):
                dto.setStatus("Public")
            elif (data[0][2] == "1"):
                dto.setStatus("Equipe")
            else:
                dto.setStatus("Prive")
            data.pop(0)
             # split the tiles
            for i in range(len(data)):
                data[i] = data[i].split()
            # its column based so we need to rotate the array

            finalData=[]



            dto.setMapData(list(data))
            return dto

    def write(self, dto):
        self.con = cx_Oracle.connect('Main', 'Dietwater', dsn_tns)
        if (self.con == None):
            print("Error, database cannot be reached")
            return
        cur = self.con.cursor()
        nom = dto.getMapNom()
        date = dto.getMapDateCreation()
        delai = dto.getDelaisItem()
        status = dto.getMapStatus()
        spawn1=dto.getSpawn1()
        spawn2=dto.getSpawn2()
        new_id = cur.var(cx_Oracle.NUMBER)
        cur.execute("select seq_map_id.nextval from dual")
        result = cur.fetchone()
        result = result[0]
        statement = """ insert into maps(nom,dateCreation,status,delaisItem,ID,spawn1_x,spawn1_y,spawn2_x,spawn2_y) values(:1,:2,:3,:4,:5,:6,:7,:8,:9) returning id into :10"""

        cur.execute(statement, (nom, date, status, int(delai['min']), int(result),spawn1['x'],spawn1['y'],spawn2['x'],spawn2['y'], new_id))
        for y in range(len(dto.getMapData())):
            for x in range(len(dto.getMapData()[y])):
                statement = """ insert into mapsData ( id_map,x,y,tile ) values (:1,:2,:3,:4)"""
                cur.execute(statement, (new_id, x, y, int(dto.getMapData()[y][x])))
        cur.execute("commit")

    def readDb(self):
        self.con = cx_Oracle.connect('Main', 'Dietwater', dsn_tns)
        cur = self.con.cursor()
        cur.execute("select ID,nom,dateCreation,status,delaisItem,spawn1_x,spawn1_y,spawn2_x,spawn2_y from maps")
        dtos = []
        maps = cur.fetchall()
        for map in maps:
            cur.execute("SELECT x,y,tile from mapsData WHERE id_map = '" + str(map[0]) + "'")
            AllMapData = cur.fetchall()
            print(AllMapData)
            mapData = [[]]
            xSize = 0
            ySize = 0
            i = 0
            while True:
                i += 1
                if (AllMapData[i][0] == 0 and AllMapData[i][1] == 1):
                    xSize += 1
                    break
                xSize = AllMapData[i][0]
            ySize = len(AllMapData) / xSize
            compteur = 0
            for x in range(ySize):
                for y in range(xSize):
                    mapData[x].append(AllMapData[compteur][2])
                    compteur += 1
                mapData.append([])
            mapData.pop(len(mapData)-1)
           
            print(mapData)
            curDto = DTOMaps.DTOMaps()
            curDto.setMapNom(map[1])
            curDto.setMapDateCreation(map[2])
            curDto.setStatus(map[3])
            curDto.setMapData(mapData)
            curDto.setDelaisItem(map[4],map[4])
            curDto.setSpawn1(map[5],map[6])
            curDto.setSpawn2(map[7],map[8])
            curDto.setMapSizeY(xSize)
            curDto.setMapSizeX(ySize)
            dtos.append(curDto)
        return dtos