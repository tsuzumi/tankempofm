CREATE TABLE balance(
	donne VARCHAR2(100),
	valeur FLOAT,
	minimum FLOAT,
	maximum FLOAT
);

CREATE TABLE messages(
	donne VARCHAR2(100),
	valeur VARCHAR2(1000),
	minimum NUMBER,
	maximum NUMBER
);

INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('vitesseChar', 4.0, 12.0, 7.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('vitesseRotationChar', 1000.0, 2000.0, 1500.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('pointsVieChar', 100.0, 2000.0, 200.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('tempsMouvementBloc', 0.2, 2.0, 0.8);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('canonVitesseBalle', 4.0, 30.0, 14.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('canonTempsRecharge', 0.2, 10.0, 1.2);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('mitrailleteVitesseBalle', 4.0, 30.0, 18.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('mitrailletteTempsRecharge', 0.2, 10.0, 0.4);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('grenadeVitesseInitialeBalle', 10.0, 25.0, 16.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('grenadetempsRecharge', 0.2, 10.0, 0.8);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('shotgunVitesseBalle', 4.0, 30.0, 13.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('shotgunOuvertureFusil', 0.1, 1.5, 0.4);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('shotgunTempsRecharge', 0.2, 10,1.8);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('piegeVitesseBalle', 0.2, 4.0, 1.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('piegeTempsRecharge', 0.2, 10.0, 0.8);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('missileGuideVitesseBalle', 20.0, 40.0, 30.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('missileGuideTempsRecharge', 0.2, 10.0, 3.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('springVitesseInitialeSaut', 6.0, 20.0, 10.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('springTempsRecharge', 0.2, 10.0, 0.5);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('rayonExplosionBalles', 1.0, 30.0, 8.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('messageAccueilDuree', 1.0, 10.0, 3.0);
INSERT INTO balance ( donne, minimum, maximum, valeur) VALUES ('messageCompteRebour', 0.0, 10.0, 3.0);

INSERT INTO messages ( donne, valeur, minimum, maximum ) VALUES ('messageSignalDebutContenu', 'bonjour', 60, 70 );
INSERT INTO messages ( donne, valeur, minimum, maximum) VALUES ('messageFinPartieContenu', 'bye bye', 50, 80);
INSERT INTO messages ( donne, valeur, minimum, maximum) VALUES ('messageAccueilContenu', 'hello', 70, 90);
commit;