
import dependance
from DAOOracle import *
from DAOCsv import *
import Tkinter as tk
from tkFileDialog import *
from os.path import expanduser
def updateCsv():
	#ask user where  we should  put the file

	home = expanduser("~")
	home=home+"\Desktop"

	root = tk.Tk()
	root.withdraw()
	file_path = asksaveasfilename(defaultextension='.csv',filetypes = [('csv files', '.csv')],initialdir=home,initialfile="balance.csv")
	csv=DAOCsv()
	oracle=DAOOracle()
	csv.create(oracle.read(),file_path)
def updateDB():
	csv=DAOCsv()
	oracle=DAOOracle()
	home = expanduser("~")
	home=home+"\Desktop"
	root = tk.Tk()
	root.withdraw()
	file_path = askopenfilename(defaultextension='.csv',filetypes = [('csv files', '.csv')],initialdir=home,initialfile="balance.csv")
	dto=csv.read(file_path)
	oracle.write(dto)

