class DTOMaps:
    def __init__(self):
        self.mapInfo = {"id":0,
                        "nom":"nom de la map",
                        "dateCreation":"la date",
                        "status": "le status",
                        "delaisItemMin":0,
                        "delaisItemMax":0,
                        "spawn1_x":0,
                        "spawn1_y":0,
                        "spawn2_x":0,
                        "spawn2_y":0,
                        "sizeX":0,
                        "sizeY":0}
        
        self.mapData = []
        self.mapData.append([0,0,0,0,0,0])
        self.mapData.append([0,0,0,0,0,0])
        self.mapData.append([0,0,2,0,0,0])
        self.mapData.append([0,0,0,3,0,0])
        self.mapData.append([0,0,0,0,0,0])
        self.mapData.append([0,0,0,0,0,0])
        
    def setMapSizeX(self,size):
        self.mapInfo["sizeX"] = size
        
    def setMapSizeY(self,size):
        self.mapInfo["sizeY"] = size
        
    def setMapId(self,id):
        self.mapInfo["id"] = id
        
    def setMapNom(self,nom):
        self.mapInfo["nom"] = nom
        
    def setMapDateCreation(self,date):
        self.mapInfo["dateCreation"] = date
        
    def setStatus(self,status):
        self.mapInfo["status"] = status
        
    def setDelaisItem(self,min,max):
        self.mapInfo["delaisItemMin"] = min
        self.mapInfo["delaisItemMax"] = max
    def setSpawn1(self,x,y):
        self.mapInfo["spawn1_x"] = x
        self.mapInfo["spawn1_y"] = y
    def setSpawn2(self,x,y):
        self.mapInfo["spawn2_x"] = x
        self.mapInfo["spawn2_y"] = y
    def setMapData(self,data):
        self.mapData = data
        
    def getMapId(self):
        return self.mapInfo.get("id")

    def getMapNom(self):
        return self.mapInfo.get("nom")
    
    def getMapDateCreation(self):
        return self.mapInfo.get("dateCreation")
    
    def getMapStatus(self):
        return self.mapInfo.get("status")
    
    def getDelaisItem(self):
        min = self.mapInfo.get("delaisItemMin")
        max = self.mapInfo.get("delaisItemMax")
        delais = {"min":min,"max":max}
        return delais
    
    def getMapData(self):
        return self.mapData
        
    def getSpawn1(self):
        x = self.mapInfo.get("spawn1_x")
        y = self.mapInfo.get("spawn1_y")
        spawn = {"x":x,"y":y}
        return spawn
        
    def getSpawn2(self):
        x = self.mapInfo.get("spawn2_x")
        y = self.mapInfo.get("spawn2_y")
        spawn = {"x":x,"y":y}
        return spawn
        
    def getMapSizeX(self):
        return self.mapInfo.get("sizeX")
        
    def getMapSizeY(self):
        return self.mapInfo.get("sizeY")
        

