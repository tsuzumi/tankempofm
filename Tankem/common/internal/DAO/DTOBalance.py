
class DTOBalance:
    def __init__(self):
		self.CURRENT_VALUE=0
		self.MIN_VALUE=1
		self.MAX_VALUE=2
		self.DEFAULT_VALUE=3
        # (currentValue,minValue,maxValue,defaultValue)
		self.dictBalanceNum = {"vitesseChar": [7, 4, 12, 7], "vitesseRotationChar": [1500, 1000, 2000, 1500],
                               "pointsVieChar": [150,100, 2000,150],
                               "tempsMouvementBloc": [0.8, 0.2, 2, 0.8],
                               "canonVitesseBalle": [14, 4, 20, 14], "canonTempsRecharge": [1.2, 0.2, 10, 1.2],
                               "mitrailleteVitesseBalle": [18.0, 4.0, 30.0, 18.0],
                               "mitrailletteTempsRecharge": [0.4, 0.2, 10.0, 0.4],
                               "grenadeVitesseInitialeBalle": [16.0, 10.0, 25.0, 16.0],
                               "grenadetempsRecharge": [0.8, 0.2, 10.0, 0.8], "shotgunVitesseBalle": [13.0,4.0, 30.0, 13.0],
                               "shotgunOuvertureFusil": [0.4, 0.1, 1.5, 0.4], "shotgunTempsRecharge": [1.8, 0.2, 10.0, 1.8],
                               "piegeVitesseBalle": [1.0, 0.2, 4.0, 1.0],
                               "piegeTempsRecharge": [0.8,0.2, 10.0, 0.8], "missileGuideVitesseBalle": [30.0,20.0, 40.0, 30.0],
                               "missileGuideTempsRecharge": [3.0,0.2, 10.0, 3.0],
                               "springVitesseInitialeSaut": [10.0,6.0, 20.0, 10.0],
                               "springTempsRecharge": [0.5,0.2, 10.0, 0.5], "rayonExplosionBalles": [8.0,1.0, 30.0, 8.0],
                               "messageAccueilDuree": [3.0, 1.0, 10.0, 3.0], "messageCompteRebour": [3.0, 0.0, 10.0, 3.0]}
		self.dictBalanceString = {"messageSignalDebutContenu": "Go", "messageFinPartieContenu": "bby bae",
                                  "messageAccueilContenu": "hello"

                                  }

    def getNum(self, nomChamp):
        if nomChamp in self.dictBalanceNum:
            return self.dictBalanceNum[nomChamp][0]
        return None

    def getText(self, nomChamp):
        if nomChamp in self.dictBalanceString:
            return self.dictBalanceString[nomChamp]
        return None

    def getMinMax(self, nomChamp):
        if nomChamp in self.dictBalanceNum:
           
            return self.dictBalanceNum[nomChamp][self.MIN_VALUE], self.dictBalanceNum[nomChamp][self.MAX_VALUE]
        return None

    def getDefaultNum(self, nomChamp):
        if nomChamp in self.dictBalanceNum:
            return self.dictBalanceNum[nomChamp][self.DEFAULT_VALUE]
        return None

    def getDefaultText(self, nomChamp):
        if nomChamp in self.dictBalanceString:
            return self.dictBalanceString[nomChamp][self.DEFAULT_VALUE]
        return None
    def setNum(self,nomChamp,newValue):
		if nomChamp in self.dictBalanceNum:
			if(newValue<self.dictBalanceNum[nomChamp][self.MIN_VALUE]):
				print("Value error, too small for : " + nomChamp)
				return False
			elif(newValue>self.dictBalanceNum[nomChamp][self.MAX_VALUE]):
				print("Value error, too big for : " + nomChamp)
				return False
			else:
				self.dictBalanceNum[nomChamp][self.CURRENT_VALUE]=newValue
				return True


    def setText(self,nomChamp,newValue):
        if nomChamp in self.dictBalanceString:
            self.dictBalanceString[nomChamp]=newValue
        else:
            pass
        
    def getNamesNum(self):
        return self.dictBalanceNum.keys()
    
    def getNamesText(self):
        return self.dictBalanceString.keys()
