

import csv
from DTOBalance import *
class DAOCsv:
	def create(self,dbInfo,file_path):
		numberNames=dbInfo.getNamesNum()
		stringNames=dbInfo.getNamesText()
		#write the received dto inside a csv file
		with open(file_path, 'wb') as csvfile:		
			spamwriter = csv.writer(csvfile, delimiter=';')
			spamwriter.writerow(["sep=;"])
			for name in numberNames:
				 spamwriter.writerow([name,dbInfo.getNum(name)])
			for name in stringNames:
				spamwriter.writerow([name,dbInfo.getText(name)])
		print("Success, file created at : "+ file_path)
	def read(self,file_path):
		dbInfo=DTOBalance()
		#create the dto based on the csv
		with open(file_path, 'rb') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=';')
			for row in spamreader:
				if len(row)>1:				
					if(dbInfo.setNum(row[0],row[1])==False):
						return None
					dbInfo.setText(row[0],row[1])
		return dbInfo
			
		


